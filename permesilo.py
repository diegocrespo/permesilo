import argparse
import os
from pathlib import Path
from datetime import datetime

LICENSES = {
    "gplv3": "GPLv3_LICENSE",
    "lgpl": "LGPL_LICENSE",
    "mit": "MIT_LICENSE",
    "apache2.0": "APACHE2_LICENSE"
}


def create_module_directory(module_name):
    """
    Creates the main module directory and subdirectories.

    :param module_name: Name of the module to create.
    """
    os.makedirs(f"{module_name}/tests", exist_ok=True)
    Path(f"{module_name}/__init__.py").touch()
    Path(f"{module_name}/{module_name}.py").touch()
    Path(f"{module_name}/tests/test_{module_name}.py").touch()


def create_readme(module_name, author_name, license_name):
    """
    Creates the README.md file with the specified content.

    :param module_name: Name of the module.
    :param author_name: Name of the author.
    :param license_name: Name of the license.
    """
    readme_content = f"""# {module_name}

This is a program written by {author_name}
## INSTALLATION

This is where the installation instructions will go
## RUNNING TESTS

Here is how to run the tests

## REDISTRIBUTION LICENSE

This project is licensed under the {license_name}
"""
    with open(f"{module_name}/README.md", "w") as readme_file:
        readme_file.write(readme_content)


def customize_license_text(license_path, author_name):
    """
    Customizes the license text with the current year and author's name.

    :param license_path: Path to the license template.
    :param author_name: Name of the author.
    :return: Customized license text.
    """
    current_year = datetime.now().year
    with open(license_path, "r") as file:
        license_text = file.read()

    # Replace placeholders with actual values
    license_text = license_text.replace("[yyyy]", str(current_year))
    license_text = license_text.replace("[name of copyright owner]", author_name)
    license_text = license_text.replace("[year]", str(current_year))
    license_text = license_text.replace("[fullname]", author_name)

    return license_text


def copy_license(module_name, license_choice, author_name):
    """
    Copies the chosen license file to the module directory and customizes it.

    :param module_name: Name of the module.
    :param license_choice: The chosen license.
    :param author_name: Name of the author.
    """
    license_file = LICENSES.get(license_choice.lower())
    if license_file:
        license_path = f"LICENSES/{license_file}"
        customized_license = customize_license_text(license_path, author_name)
        with open(f"{module_name}/LICENSE", "w") as license_out:
            license_out.write(customized_license)


def main():
    parser = argparse.ArgumentParser(description="Create a Python module with a specified structure.")
    parser.add_argument("module_name", help="Name of the module to create")
    parser.add_argument("author_name", help="Name of the author")
    parser.add_argument("license_choice", choices=LICENSES.keys(),
                        help="License for the module (gplv3, lgpl, MIT, apache2.0)")

    args = parser.parse_args()

    create_module_directory(args.module_name)
    create_readme(args.module_name, args.author_name, args.license_choice)
    copy_license(args.module_name, args.license_choice, args.author_name)

    print(f"Module {args.module_name} created successfully with a {args.license_choice} license.")


if __name__ == "__main__":
    main()
