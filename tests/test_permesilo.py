import os
import shutil
import unittest

from permesilo.permesilo import create_module_directory, copy_license


class TestModuleCreation(unittest.TestCase):

    def setUp(self):
        # Setup for each test
        self.module_name_gpl = "test_module_gplv3"
        self.module_name_mit = "test_module_mit"
        self.author_name = "Test Author"

    def test_gplv3_module_creation(self):
        # Test module creation with GPLv3 license
        create_module_directory(self.module_name_gpl)
        copy_license(self.module_name_gpl, "gplv3", self.author_name)

        # Check if the directory structure is correct
        self.assertTrue(os.path.isdir(self.module_name_gpl))
        self.assertTrue(os.path.isfile(f"{self.module_name_gpl}/__init__.py"))
        self.assertTrue(os.path.isfile(f"{self.module_name_gpl}/{self.module_name_gpl}.py"))
        self.assertTrue(os.path.isdir(f"{self.module_name_gpl}/tests"))
        self.assertTrue(os.path.isfile(f"{self.module_name_gpl}/tests/test_{self.module_name_gpl}.py"))
        self.assertTrue(os.path.isfile(f"{self.module_name_gpl}/LICENSE"))

        # Check if the correct license file is copied
        with open(f"{self.module_name_gpl}/LICENSE", "r") as file:
            content = file.read()
        self.assertIn("GNU GENERAL PUBLIC LICENSE", content)

    def test_mit_module_creation(self):
        # Test module creation with MIT license
        create_module_directory(self.module_name_mit)
        copy_license(self.module_name_mit, "mit", self.author_name)

        # Check if the directory structure is correct
        self.assertTrue(os.path.isdir(self.module_name_mit))
        self.assertTrue(os.path.isfile(f"{self.module_name_mit}/__init__.py"))
        self.assertTrue(os.path.isfile(f"{self.module_name_mit}/{self.module_name_mit}.py"))
        self.assertTrue(os.path.isdir(f"{self.module_name_mit}/tests"))
        self.assertTrue(os.path.isfile(f"{self.module_name_mit}/tests/test_{self.module_name_mit}.py"))
        self.assertTrue(os.path.isfile(f"{self.module_name_mit}/LICENSE"))

        # Check if the correct license file is copied
        with open(f"{self.module_name_mit}/LICENSE", "r") as file:
            content = file.read()
        self.assertIn("MIT License", content)

    def tearDown(self):
        # Cleanup after each test
        shutil.rmtree(self.module_name_gpl, ignore_errors=True)
        shutil.rmtree(self.module_name_mit, ignore_errors=True)


if __name__ == '__main__':
    unittest.main()
